# coding=utf-8%
from common_imports import *

@checkRoles(min_role = 0)
def privilegi(command, people, id_map):
    bot.sendMessage(command.chat_id, people[command.fid]['sname']+', i tuoi privilegi: '+people[command.fid]['privilegi'])
    return 0
