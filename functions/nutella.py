# coding=utf-8%                              
from common_imports import *

@checkRoles(min_role = 0)
def nutella(command, people, id_map, riscatta_flag=False):
    u"""/nutella <n> \n\npaga <n> cucchiai di nutella (a s.t.p.) ! (<n> = 1 se omesso)"""
    n_cucchiai = 1
    if len(command.message.split()) > 1:
        try:
            if check_nan(command, people, id_map, 1):
                return 1
            else:
                n_cucchiai = int(command.message.split()[1])
        except:
            bot.sendMessage(command.chat_id, "Il numero di cucchiai di nutella che vuoi deve essere un numero, tacchino.")
            return 1
    if not riscatta_flag:
        people[command.fid]['saldo']-=people[command.fid]['ban_score'][0]*(0.60*n_cucchiai)
        people[command.fid]['punti']+=0.60*n_cucchiai
        str_message='Grande '+str(people[command.fid]['sname'])+u', hai pagato per '+str(n_cucchiai)+' cucchiai di felicità! Il tuo saldo è adesso pari a '+str(people[command.fid]['saldo'])+u'\u20AC.'
    if riscatta_flag:
        str_message='Grande '+str(people[command.fid]['sname'])+u', cucchiao di nutella riscattato! Il tuo saldo rimane a '+str(people[command.fid]['saldo'])+u'\u20AC.'
    bot.sendMessage(command.chat_id, str_message)
    return 0
