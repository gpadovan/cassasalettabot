# coding=utf-8%     
from common_imports import *
import functions

@checkRoles(min_role = 0)
def help(command, people, id_map):
    if len(command.message.split())>1:
        try:
            command.set_role("Presidente")
            print( getattr(getattr(functions, command.message.split()[1]), command.message.split()[1]).__doc__)
            print(getattr(functions, command.message.split()[1]))
            bot.sendMessage(command.chat_id, getattr(getattr(functions, command.message.split()[1]), command.message.split()[1]).__doc__)
        except:
            bot.sendMessage(command.chat_id, 'Errore. La funzione non eiste o il suo help non è ancora stato implementato.\nDigita /help per una lista delle funzioni di cui è disponibile una spiegazione.')
            return 1
    else:
        functions_with_help=['saldo', 'paga', 'ricarica', 'trasferimento', 'caffe', 'pasta', 'contabilizza', 'saldoSaletta', 'spesa','preleva', 'netto', 'spaga', 'spasta', 'scaffe', 'riscatta', 'nomina', 'ricompensa', 'contatore', 'shame', "CMS", "CMSmmerda", "slot", "punti", "sticker", "updateKeyboard", "cappuccino", "sticker", "polaroid", "crepe", "nutella"]
        
        message="Non sai cosa fare?\nEccoti le funzioni per cui è disponibile una guida. Digita: /help <nome funzione> per ottenerne\
 spiegazione e sintassi.\n\n"
        for func in functions_with_help:
            message+=func
            message+='\n'
        message+="\nOgni funzione deve essere usata responsabilmente. Ogni trasgressione sarà punita a norma di legge."
        bot.sendMessage(command.chat_id, message)
    
        return 0
