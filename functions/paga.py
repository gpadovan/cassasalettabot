from common_imports import *

@checkRoles(min_role = 0)
def paga(command, people, id_map,  disable_message=False, riscatta_flag=0):
    u"""/paga <importo> <causale>\n\nsottrae <importo> dal saldo annotando la <causale>"""
    
    if check_nan(command, people, id_map, 1):
        return 1
    try:
        if float(command.message.split()[1])>=0:
            c=abs(float(command.message.split()[1]))
        else:
            bot.sendMessage(command.chat_id, 'Importi negativi non consentiti.')
            return 1
        pay_message=''
        for i, w in enumerate(command.message.split()[2:]):
            pay_message+=w
            if i!=len(command.message.split()[2:])-1:#evita di aggiungere uno spazio in fondo
                pay_message+=' '
        
    except:
        if not disable_message:
            bot.sendMessage(command.chat_id, 'Importo selezionato non valido o messaggio di specifica dimenticato!\nDigita /paga <importo> <messaggio> per sganciare quanto devi!')
            return 1
        
    costo=people[command.fid]['ban_score'][0]*c
    
    people[command.fid]['paga'].append([costo, command.date])
    if not riscatta_flag:
        people[command.fid]['saldo']-=costo
        people[command.fid]['punti']+=c
        bot.sendMessage(command.chat_id, people[command.fid]['sname'] + ' ha pagato '+str(c*people[command.fid]['ban_score'][0])+u'\u20AC  per '+pay_message+'! Saldo rimanente: '+str(people[command.fid]['saldo'])+u'\u20AC.')
    return 0
    
