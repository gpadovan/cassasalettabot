# coding=utf-8%                              
from common_imports import *

@checkRoles(min_role = 0)
def macchiato(command, people, id_map, riscatta_flag=False):
    u"""/macchiato\n\npaga macchiato"""
    people[command.fid]['caffe'].append(command.date)
    today_coffees=0
    for date in people[command.fid]['caffe']:
        if date.day==command.date.day and date.month==command.date.month and date.year==command.date.year:
            today_coffees+=1
    if not riscatta_flag:
        people[command.fid]['saldo']-=people[command.fid]['ban_score'][0]*0.45
        people[command.fid]['punti']+=0.45
        str_message='Grande '+str(people[command.fid]['sname'])+u', macchiato pagato! Il tuo saldo è adesso pari a '+str(people[command.fid]['saldo'])+u'\u20AC.'
    if riscatta_flag:
        str_message='Grande '+str(people[command.fid]['sname'])+u', macchiato riscattato! Il tuo saldo rimane a '+str(people[command.fid]['saldo'])+u'\u20AC.'
    if today_coffees>2:
        str_message+=u' Occhio che oggi già ne hai presi '+str(today_coffees)+'.'
    if today_coffees==3:
        bot.sendVideo(command.chat_id, 'https://media.giphy.com/media/S5VnxByabCtLVgGv4N/giphy.gif')
    if today_coffees==4:
        bot.sendVideo(command.chat_id, 'https://media.giphy.com/media/LPr6GAWziTwNnZW6R6/giphy.gif')
    if today_coffees>=5:
        bot.sendVideo(command.chat_id, 'https://media.giphy.com/media/l0zyoEDlsUVrtvrFDV/giphy.gif')
    bot.sendMessage(command.chat_id, str_message)
    return 0
