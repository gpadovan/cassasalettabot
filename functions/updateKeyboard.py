from common_imports import *

@checkRoles(min_role = 1)
def updateKeyboard(command, people, id_map):
    u"""/updateKeyboard\n\nAggiorna la tastiera."""
    bot.sendMessage(command.chat_id, 'Aggiornamento tastiera in corso.',
                    reply_markup=start_keyboard
                    )
    return 0
