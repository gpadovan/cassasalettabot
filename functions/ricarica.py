from common_imports import *

@checkRoles(min_role = 0)
def ricarica(command, people, id_map):
    u"""/ricarica <importo>\naggiunge <importo> al saldo. Da usare quando si versano soldi in contanti in cassa"""
    try:
        if check_nan(command, people, id_map, 1):
            return 1
        if float(command.message.split()[1])>=0:
            r = abs(float(command.message.split()[1]))
        else:
            bot.sendMessage(command.chat_id, 'Importi negativi non consentiti.')
            return 1

    except:
        bot.sendMessage(command.chat_id, 'Importo selezionato non valido!\nQuanto vuoi ricaricare?\nScrivi /ricarica <importo>')
        return 1
    people[command.fid]['saldo']+=r
    people['CassaSaletta']['saldo']+=r
    bot.sendMessage(command.chat_id, people[command.fid]['sname']+' ha ricaricato '+str(r)+ u'\u20AC.  Saldo attuale: '+str(people[command.fid]['saldo'])+u'\u20AC.')
    return 0
