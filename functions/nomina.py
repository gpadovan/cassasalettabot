# coding=utf-8%
from common_imports import *

@checkRoles(min_role = 2)
def nomina(command, people, id_map):
    u"""/nomina <nome saletta> <privilegi>\n\nCambia i privilegi di <nome saletta> in <privilegi>. Sono necessari privilegi particolari per usare questa funzione."""

    person = id_map[command.message.split()[1]]['id']
    try:
        person=id_map[command.message.split()[1]]['id']
    except:
        bot.sendMessage(command.chat_id, 'Errore, nome scorretto o non esistente.')
        return 1
    people[person]['privilegi']=command.message.split()[2]
    bot.sendMessage(command.chat_id, people[person]['sname']+', sei stato nominato ' + people[person]['privilegi']+'. Porta con onore questo titolo.')

    return 0
