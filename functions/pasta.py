from common_imports import *

@checkRoles(min_role = 0)
def pasta(command, people, id_map, riscatta_flag=False):
    u"""/pasta <importo>\n\npaga una pasta che costa <importo>"""
    if len(command.message.split())!=1:
        try:
            if check_nan(command, people, id_map, 1):
                return 1
            if float(command.message.split()[1])>=0:
                c=abs(float(command.message.split()[1]))
            else:
                bot.sendMessage(command.chat_id, 'Importi negativi non consentiti.')
                return 1
            
        except:
            bot.sendMessage(command.chat_id, 'Importo selezionato non valido!\nDigita /pasta <importo> per sganciare quanto devi!')
            return 1
        people[command.fid]['pasta'].append([c, command.date])

        if not riscatta_flag:
            people[command.fid]['saldo']-=people[command.fid]['ban_score'][0]*c
            people[command.fid]['punti']+=c
            bot.sendMessage(command.chat_id, 'Pagamento pasta avvenuto con successo.\nIl saldo di '+people[command.fid]['sname']+' passa da '+str(people[command.fid]['saldo']+c*people[command.fid]['ban_score'][0])+' a '+str(people[command.fid]['saldo'])+'.', reply_markup=start_keyboard)
        return 0
    elif len(I.split())==1:
        bot.sendMessage(command.chat_id, 'Pagamento pasta...',
                        reply_markup=pasta_keyboard
                        )
