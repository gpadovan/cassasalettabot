from common_imports import *

@checkRoles(min_role = 0)
def saldoSaletta(command, people, id_map):
    u"""/saldoSaletta\n\nRestituisce la somma di denaro presente nelle casse del tesoro"""
    bot.sendMessage(command.chat_id, "La saletta ha un saldo di "+str(people['CassaSaletta']['saldo'])+u'\u20AC')
    return 0
