from common_imports import *
#from command import *
def check_nan(command, people, id_map, n):
    try:
        if math.isnan(float(command.message.split()[n])) or math.isinf(float(command.message.split()[n])):
            bot.sendMessage(command.chat_id, 'Hai provato a fregare il sistema eh? Violazione rilevata. Importo non valido.')
            temp_command=command('/ban '+command.sname, command.fid, command.name, command.chat_id, command.sname)
            ban(temp_command, people, chat_id)
            return 1
        else:
            return 0
    except:
        bot.sendMessage(command.chat_id, 'Errore.')
