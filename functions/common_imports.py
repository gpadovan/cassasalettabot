# coding=utf-8%                              
import telepot
from functools import wraps
from telepot.namedtuple import ReplyKeyboardMarkup, KeyboardButton
TOKEN='631352319:AAGXSJaQD8HO7iI18sdV3MgIaA7VqELLpcA'
bot = telepot.Bot(TOKEN)

#TOKEN='1042986801:AAEW5sHcPOZVWUMHo4iOUsrFlymI71oSJmQ'
#bot = telepot.Bot(TOKEN)
start_keyboard=ReplyKeyboardMarkup(
    keyboard=[
        [KeyboardButton(text="/sticker"),KeyboardButton(text="/netto"),KeyboardButton(text="/help")],[KeyboardButton(text="/caffe"),  KeyboardButton(text="/punti"),KeyboardButton(text="/slot")]
        ]
    )

import numpy as np
import os
import sys
import time
import unicodedata
import numpy as np
import datetime
from telepot.loop import MessageLoop
import math
from check_nan import *
from return_fid import *
#from caffe import *
#from pasta import*
#from paga import *
import random
from Slot_machine import *
from pprint import pprint

RoleMap = {"presidente": 2,
           "senatoreavita": 1,
           "ministrodellefinanze": 0,
           "paesano": 0,
           "boss": 0
           }

def checkRoles(min_role):

    def decorator(func):
        #f"""{func.__doc__}"""
        @wraps(func)
        def wrapper(*args, **kwargs):
            #wrapper.__doc__ = func.__doc__
            role = max([RoleMap[r] for r in RoleMap if r in args[0].role.lower()])
            print(args[0].role, role)
            if  role >= min_role:
                func(*args, **kwargs)
            else:
                bot.sendMessage(args[0].chat_id, "Non hai i privilegi necessari")
                return 0

        return wrapper
    return decorator
