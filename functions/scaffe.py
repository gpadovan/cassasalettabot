from common_imports import *

@checkRoles(min_role = 0)
def scaffe(command, people, id_map, disable_message=False):
    u"""/scaffe\n\nAumenta il saldo personale del costo di un caffe' ed elimina l'ultimo caffe', senza toccare il saldo della cassa, a differenza di ricarica. E' possibile annullare un caffe solo entro 15 min da quando e' stato preso."""
    spaga_flag=1
    if people[command.fid]['caffe'][-1].day==command.date.day and people[command.fid]['caffe'][-1].month==command.date.month and (command.date-people[command.fid]['caffe'][-1]).seconds<(15*60):
        people[command.fid]['saldo']+=0.35
        people[command.fid]['punti']-=0.35
        people[command.fid]['caffe'].pop(-1)
        bot.sendMessage(command.chat_id, people[command.fid]['sname'] + " ha annullato il pagamento dell'ultimo caffe'! Il saldo torna a: "+str(people[command.fid]['saldo'])+u'\u20AC.')
        spaga_flag=0

    
    if spaga_flag:
        bot.sendMessage(command.chat_id, people[command.fid]['sname'] + " Negli ultimi 15 minuti pare che tu non abbia preso un caffe'. Stai provando a rubacchiare per caso? Il ban e' dietro l'angolo.")
        
    return 0
