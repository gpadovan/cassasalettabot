# coding=utf-8%
from common_imports import *
def start(command, people, id_map):

    try:
        if len(command.message.split())>3 or len(command.message.split())<2:
            bot.sendMessage(command.chat_id, 'Inizializzazione non riuscita! Se vuoi essere inizializzato a zero digita /start <NomeSaletta> . Altrimenti digita /start <importo> <NomeSaletta> (<NomeSaletta> deve essere una sola parola)\nN.B Tu non potrai cambiare <NomeSaletta> (ma io si). Quindi bada bene a cosa scegli')
            return 1
        if len(command.message.split())==3:
            if check_nan(command, people, id_map, 1):
                return 1
            sname=command.message.split()[2]
            saldo=float(command.message.split()[1])

        elif len(command.message.split())==2:
            if command.message.split()[1].isnumeric():
                bot.sendMessage(command.chat_id, 'Hai dimenticato di inserire il tuo NomeSaletta. Se vuoi essere inizializzato a zero digita /start <NomeSaletta> . Altrimenti digita /start <importo> <NomeSaletta> (<NomeSaletta> deve essere una sola parola)\nN.B Tu non potrai cambiare <NomeSaletta> (ma io si). Quindi bada bene a cosa scegli')
                return 1
            else:
                saldo=0
                sname=command.message.split()[1]
        print("YE")
        for person in people:
            if person!='CassaSaletta':
                if people[person]['sname']==sname:
                    bot.sendMessage(command.chat_id, sname+ u' esiste già. Scegline un altro.')
                    return 1
    except:
        bot.sendMessage(command.chat_id, 'Inizializzazione non riuscita! Se vuoi essere inizializzato a zero digita /start <NomeSaletta> . Altrimenti digita /start <importo> <NomeSaletta> (<NomeSaletta> deve essere una sola parola)\nN.B Tu non potrai cambiare <NomeSaletta> (ma io si). Quindi bada bene a cosa scegli')
        return 1
    if command.fid not in people:
        people[command.fid]={'nome':command.name, 'id':command.fid, 'sname':sname, 'saldo':saldo, 'privilegi': 'paesano', 'caffe':[], 'pasta':[], 'paga':[], 'CMS':False, 'punti':0, 'ban_score':[1, command.date]}
        id_map[sname]={'id':command.fid}
        np.save('id_map.npy', id_map)
        bot.sendMessage(command.chat_id, people[command.fid]['sname']+' sei stato aggiunto/a alla lista con saldo di '+str(people[command.fid]['saldo'])+u'\u20AC.',
                                reply_markup=start_keyboard
                                )
        return 0
    else:
        bot.sendMessage(command.chat_id, people[command.fid]['sname']+ u', già esisti!')
        return 1
