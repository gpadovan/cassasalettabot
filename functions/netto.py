from common_imports import *

@checkRoles(min_role = 0)
def netto(command, people, chat_id):
    u"""/netto\n\nRestituisce il netto delle casse reali."""
    netto=0
    for person in people:
        if person!='CassaSaletta':
            netto-=people[person]['saldo']
        else:
            netto+=people[person]['saldo']
    bot.sendMessage(command.chat_id, "Il netto della saletta e' pari a "+str(netto)+ u'\u20AC.')

    return 0
