# coding=utf-8%
from common_imports import *

@checkRoles(min_role = 0)
def matteo(command, people, id_map):
    u"""/matteo\n\nAh no?"""
    bot.sendVideo(command.chat_id, 'https://c.tenor.com/PGmc9OwoVR4AAAAC/ah-no-non-posso.gif')
    return 0
