import functions
import datetime
class command():
    def __init__(self, message, fid, name, chat_id, date, sname=''):
        self.message = message
        self.fid = fid
        self.name = name
        self.sname = sname
        self.chat_id = chat_id
        self.date = date
        self.func = getattr(getattr(functions, self.message.split()[0].strip('/')), self.message.split()[0].strip('/'))
        
    def execute(self, people, id_map):
        if(self.message.split()[0].strip('/')!='start'):
            self.role = people[self.fid]["privilegi"]
        else:
            self.role = "paesano"
        result = self.func(self, people, id_map)
        return(result)
    def set_message(self, message):
        self.message = message
    def set_fid(self, fid):
        self.fid = fid
    def set_sname(self, sname):
        self.sname = sname
    def set_name(self, name):
        self.name = name
    def set_role(self, role):
        self.role = role
